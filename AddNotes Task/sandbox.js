/* Note:
    I used Bootstrap for create task part and not for the accordion. 
    If same notes fields are displayed for multiple times just refresh the window one more time.

*/





const config = {
    apiKey: "AIzaSyCWmw5A1pE5WDHoRTHy95dzbY8cGsWCT_w",
    authDomain: "nextgrowth-648c0.firebaseapp.com",
    databaseURL: "https://nextgrowth-648c0-default-rtdb.firebaseio.com",
    projectId: "nextgrowth-648c0",
    storageBucket: "nextgrowth-648c0.appspot.com",
    messagingSenderId: "907014894314",
    appId: "1:907014894314:web:f2603c1ff0051cfe4f9d93",
    measurementId: "G-3Z5EWXXVBV"
};

firebase.initializeApp(config);
var db = firebase.database();


const dbRef = db.ref();






//yyyy-mm-dd
//Inserting the data
const savetask = document.getElementById("savetask")

savetask.addEventListener("click", () => {
    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;






    const taskname = document.getElementById("taskname").value
    const taskdesc = document.getElementById("taskdesc").value

    if(taskname.length > 0 && taskdesc.length > 0){
        dbRef.child('TaskApp').child(taskname).set({
            TaskName: taskname,
            TaskDescription: taskdesc,
            TaskCreated: dateTime,
            TaskMessage : 'Last Edited a Few Seconds Ago'

        })
        window.location.reload()
    }  

    else{
        document.write("Please enter valid data")
    }
    
});



//retriving the data and displaying frontend by DOM

dbRef.child('TaskApp').on('value', 
    function(alldata) {

            alldata.forEach(data => {
                var tn = data.val().TaskName;
                var td = data.val().TaskDescription;
                var tc = data.val().TaskCreated;
                var tm = data.val().TaskMessage;



                // console.log(tn, td, tc, tm);
                let div1 = document.createElement('div')
                let div2 = document.createElement('div')

                document.getElementById("main").appendChild(div1)
                document.getElementById("main").appendChild(div2)


                div1.setAttribute('class', 'main-title');
                div2.setAttribute('class', 'main-content');

                if(document.getElementById(tn)){
                    const elem = document.getElementById(tn)
                    elem.remove()
                }
                div1.setAttribute('id', tn);
               
                

                div1.innerHTML=  `<h4>${tn}</h4><p>${tm}</p>`
                div2.innerHTML = `<p>${td}</p>`

                div2.style.display = "none"
            })



    let content = document.querySelectorAll(".main-title")
    content.forEach(con => {
        con.addEventListener("click", () => {
            console.log(con.classList.contains("clicked"))
            if(con.classList.contains("clicked")){
                con.classList.remove("clicked")
                let mainc = con.nextElementSibling;
                if(mainc.style.display == "block"){
                    mainc.style.display = "none";
                }
                
            }
            else{
                console.log("this one accessed")
                con.classList.toggle("clicked");
                let mainc = con.nextElementSibling;
                if(mainc.style.display == "none"){
                    mainc.style.display = "block";
                }
                
            }
            
        })
        
    })



        }
    )
    









//searching

const search = document.getElementById("search")

search.addEventListener("keyup", e=> {
    e.preventDefault();

    let searchdata = search.value

    const searching = async() => {
        await dbRef.child('TaskApp').on('value', 
        function(alldata) {
            alldata.forEach(data => {
                var tn = data.val().TaskName;

                if(tn.indexOf(searchdata) > -1) {
                    const taskid = document.getElementById(tn)
                    taskid.style.display = "block";
                }
                else{
                    const taskid = document.getElementById(tn)
                    taskid.style.display = "none";
                }
            })
        })
    }

    searching()
})



//updating the time edited





//grabbing the current time and date
var curr_today = new Date();
var curr_date = curr_today.getFullYear()+'-'+(curr_today.getMonth()+1)+'-'+curr_today.getDate();
var curr_time = curr_today.getHours() + ":" + curr_today.getMinutes() + ":" + curr_today.getSeconds();
var CurrTime = curr_date+' '+ curr_time;

var current_date = CurrTime.split(' ')[0]
var current_time = CurrTime.split(' ')[1]
let cyear = current_date.split('-')[0]
let cmonth = current_date.split('-')[1]
let cdate = current_date.split('-')[2]

let ctime = cmonth+'/'+cdate+'/'+cyear


let chour = current_time.split(':')[0] 
let cmin = current_time.split(':')[1] 
let csec = current_time.split(':')[2] 


//grabbing the created date

let diff_time = 0
let diff_str = ""
let message = ""

dbRef.child('TaskApp').on('value', 
    function(alldata) {
        alldata.forEach(data => {
            var taskn = data.val().TaskName
            var date  = data.val().TaskCreated
            var created_date = date.split(' ')[0]
            var created_time = date.split(' ')[1]
            


            let cuhour = created_time.split(':')[0] 
            let cumin = created_time.split(':')[1] 
            let cusec = created_time.split(':')[2] 


            let cuyear = created_date.split('-')[0]
            let cumonth = created_date.split('-')[1]
            let cudate = created_date.split('-')[2]

            let cutime = cumonth+'/'+cudate+'/'+cuyear

            console.log(cutime, ctime)
            if(cutime == ctime){ //checking the dates are equal
                if(cuhour == chour){ //checking the hours are equal
                    if(cmin == cumin){
                        diff_time = Math.abs(csec - cusec)
                        diff_str = "seconds"
                    }
                    else{
                        diff_time = Math.abs(cmin - cumin)
                        if(diff_time <= 1){
                            diff_str = "minute"
                        }
                        else{
                            diff_str = "minutes"
                        }
                    }
    
                }
                else{
                    diff_time = Math.abs(chour - cuhour)
                    if(diff_time <= 1){
                        diff_str = "hour"
                    }
                    else{
                        diff_str = "hours"
                    }
                    
                }



                //update the firebase taskmessage field
                message = `Last edited on ${diff_time} ${diff_str} ago`


            }
            else{
                const date1 = new Date(cutime); 
                const date2 = new Date(ctime);
                const diffTime = Math.abs(date2 - date1);
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 

                if(diffDays <= 1){
                    diff_str = "day"
                }
                else{
                    diff_str = "days"
                }
                

                message = `Last edited on ${diffDays} ${diff_str} ago`

            }


            dbRef.child('TaskApp').child(taskn).update({
                "TaskMessage": message
            })
            
            
        })
    }) 

